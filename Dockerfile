FROM maven

RUN mvn org.apache.maven.plugins:maven-dependency-plugin:get -Dartifact=com.github.spotbugs:spotbugs-maven-plugin:3.1.1
ADD https://repo.maven.apache.org/maven2/com/h3xstream/findsecbugs/findsecbugs-plugin/1.7.1/findsecbugs-plugin-1.7.1.jar /findsecbugs-plugin.jar
COPY include.xml analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]
